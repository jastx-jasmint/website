+++
menu = "about"
title = 'About'
toc = false
+++

Hi, I'm Jasmint. :tea: 

I also go by JasTx, Jasmineteax and JasmineTx. I like doing art sometimes, and can unintentionally be a bit of a troublemaker (sorry in advance).  
I started out doing 2D "illustration", like every other noob and their dog on deviantART. 

After leveling up in real life, I learned how to use a bunch of 3D suites and game engines like Maya, Substance Painter and Designer, Zbrush, Unreal Engine and Unity, among many others. Later, I moved to Blender and back to Krita, and use Kdenlive to edit videos. 
Basically, I'm all for open source... it's tough not to be after seeing what it's like on the 3D art scene with Autodesk and Adobe. :')

**Fun fact**: I actually don't really play video games all that much. To me there are just better things to do most of the time, like learning to build some website only maybe 5 people will ever visit :D

I hope you enjoy perusing this site, though there isn't really much to see yet. I'm still working on improving this site bit-by-bit, from content to optimisation.  
It's surprisingly interesting learning about web development :o

If you have any queries or concerns, feel free to contact me using any of the means listed in the socials. If you also happen to have my Discord, Matrix or IRC, use 'em.

I use Linux btw. Sorry, just had to get that one out of my system. :D
{.smalltext}

---
**Here's an overview of the projects I maintain.** Click the titles for links to the projects!

### [Final Fantasy 11: Jasmint's HD Textures](https://www.nexusmods.com/finalfantasy11/mods/13?tab=description)

Making the in-game player character models look more like their character creation counterparts. Eventually, I plan to also overhaul other NPCs and equipment.

{{< eagerimg image="header_banner3_marked.png" alt="Header image for Jasmint's HD Textures project" >}}


### [Garuda Linux Wallpapers](https://gitlab.com/jastx-jasmint/wallpapers)

Painting up some wallpapers for Garuda Linux, and maybe other Linux distributions in the future as well.

![garuda-fire-female](https://gitlab.com/jastx-jasmint/wallpapers/-/raw/main/garuda_linux/garuda_female_fire.png)
Speedpaint: https://odysee.com/@jasmineteax:3/garuda_fire_speedpaint:2

![garuda-fire-male](https://gitlab.com/jastx-jasmint/wallpapers/-/raw/main/garuda_linux/garuda_male_fire.png?ref_type=heads)

---

## Where is this website hosted?

This website is hosted on [GitLab](https://gitlab.com/jastx-jasmint/website) using their automatic deployment stuff. I happen to have a GitLab account so why not? :D

If you'd like to help me improve this website (I'm not a web developer after all :D), feel free to submit a merge request detailing your changes. You can also send an email about it to make sure I get back to you quickly. ^^

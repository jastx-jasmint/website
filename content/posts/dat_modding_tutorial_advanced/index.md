+++
title = '[FFXI] Advanced DAT Modding Tutorial'
description = "DAT mod tutorial: advanced"
date = 2023-09-24T14:37:11+10:00
draft = false
tags = ["Final Fantasy XI", "Final Fantasy 11", "Modding", "Final Fantasy", "Art", "Tutorials"]
toc = true
+++

This tutorial series will go over my advanced DAT modding workflow for ripping, modding and previewing your Final Fantasy XI textures. 

***If you haven't gone through the [basic series](/website/posts/dat_modding_tutorial_basic), please go through that first as the knowledge there is foundational.***
{.important}

For this tutorial, I'll use a player character:  
{{< eagerimg image="mod_tut_advanced_mithra1.png" alt="Mithra texture vanilla vs. custom upscale comparison" >}}

Let's begin! :tea:

## Additional Prerequisites
- [Noesis](https://richwhitehouse.com/index.php?content=inc_projects.php&showproject=91) by Rich Whitehouse
- [Blender](https://www.blender.org/download/) (or any other 3D art software of your choice)
- [TexTools](https://github.com/SavMartin/TexTools-Blender) (optional, good for editing more than one texture at a time)

This tutorial assumes you have foundational (at the very least, enough to navigate) knowledge of your program of choice, and the core concept of UV mapping in relation to texturing.  
Ideally, your 3D software should be capable of texture painting and projection, and rigging.
If you choose not to use Blender, you may need to use additional software such as Substance Painter or 3DCoat.

n.b: This workflow has been tested on Windows and Linux, but not MacOS.
{.smalltext}

## Ripping Models and Textures
### 1. Noesis
Noesis is a great model viewer for a lot of different games. It's always good to keep it handy somewhere.

![Noesis UI overview](mod_tut_advanced_screenshot1.jpg)

**Area 1**  
Folder navigation. Choose the folder which contains the .DAT file

**Area 2**  
File navigation. Choose the .DAT file you want to open here

**Area 3**  
Viewport controls (this is tiny in the screenshot due to my HiDPI display, sorry!)

**Area 4**  
Shading toggle and other useful things.

Once you've found your asset in AltanaView or another list, open it and if Noesis prompts you for a skeleton file, use the player character itself - you get the skeleton file when you first select the player character model in AltanaView. Selecting a head afterwards gives you the head and not the skeleton.

> ***"How do I preview and export a full player character with a certain set of equipment in Noesis?"***
>
> You need to use a .ff11datset file. For your convenience, [here](https://gitlab.com/jastx-jasmint/ffxi-noesis-files/-/tree/main) are my files that contain datsets of all player characters wearing their starting gear. It also contains the character creation models.
>
> To preview and export a certain piece of equipment, simply change the DAT being pointed to in the file. To add a weapon, add this to the end of the file in the list:
>
> ```
> dat			"weapon"		"ROM/29/20.dat"
> ```
>  You can find more examples in the Noesis scenes directory.

As Noesis is a model viewer, this is also an opportunity to have a good look at the asset without other things in the way :)

*I hope you're not going insane from different control schemes of 3D programs yet...* :')
{.smalltext}

To export the model and textures, go to **file > export**. You will then be presented with this pop-up:  
![Noesis export options](mod_tut_advanced_screenshot2.jpg)

**Here's what needs to be set:**
- Set main output type to **Autodesk FBX**
- Additional texture output: **.tga** or **.png** (Noesis exports .png by default)
- **Advanced options:** 
	- `-ff11optimizegeo -ff11keepnames 1 -ff11noshiny -fbxsmoothgroups -fbxtexrel`

Removing animations is optional but I like to do it to keep a clean Blender scene file.

That's it! Noesis is quite straightforward to use. :D

From here, you can upscale your textures with chaiNNer. Since this is the advanced tutorial, I will also show some neat tricks to upscale a bit more efficiently as opposed to one at a time.

## AI Upscaling with ChaiNNer
This tutorial will cover batch AI upscaling, and builds upon the [previous upscaling tutorial](/website/posts/dat_modding_tutorial_basic#2-upscaling).

Here's my batch upscaling setup:  
![chaiNNer batch upscaling overview](mod_tut_advanced_screenshot3.jpg)

The most essential component is **'load image (iterator)'**. 
Anything you put in the box will apply to all images in the specified directory and its **subdirectories**.

***Update***  
In newer versions of chaiNNer, there is now a recursive option. Untick this and subdirectories will not be upscaled.
{.important}

To prevent upscaling any previously upscaled work, I highly recommend using multiple **'load model'**, **'upscale image'**, **'text append'** and **'save image'** nodes.
This way, you can simply run the iterator once and get all the upscaled textures you could possibly want or need. :)

If you're interested in more chaiNNer setups, take a look at chaiNNer's GitHub page. There are many more advanced setups there.

## Blender Import and Setup
If you're not using Blender, I assume you already know what you're doing, so you can move to the [next stage](#texture-painting).

### 1. Importing into Blender
With your nice new default scene (delete everything there with **A > del**), go to **file > import > FBX (.fbx)**.

Here are the import options to keep in mind (*make sure to save them as an operator preset!*):  
![Blender import options](mod_tut_advanced_screenshot4.jpg)

Enable **manual orientation**, and set to **-X forward** and **-Y up**.  
This next part is more of a nice-to-have so the bones look clean:
- **Automatic bone orientation**  
Without this, the bones will just face one direction instead of creating a legible skeleton.

![Blender automatic bone orientation import option comparison](mod_tut_advanced_mithra2.png "Without automatic orientation vs. with automatic orientation")  
*Without automatic orientation vs. with automatic orientation*
{.annotation}

After importing, it might be hard to see your asset.  
To quickly get it in view without a numpad, hit backtick (\`) and **'view selected'**.   
(If you have a numpad, you can switch views much faster :D)

Because the asset is so small, you'll need to adjust the clip start:
- **n > view > clip start**  
I recommend adding an extra zero.

![Good vs. bad clip start in Blender](mod_tut_advanced_mithra3.png "Good vs. bad clip start")  
*Good vs. bad clip start*
{.annotation}

### 2. Material Cleanup
This is required because if you switch to material preview (z or top bar), your asset will be an eye-hurting shade of pink!

Unfortunately this is a rather manual process, although there is a [script](https://blender.stackexchange.com/questions/212618/change-all-metallic-value-with-python) that can speed it up slightly for you by removing the metallic attribute.

Removing the specular isn't necessary due to the diffuse colour preview, but if the shine bugs you then feel free.

#### Here's a checklist to make sure you're on the right track:
- **Base colour** set to the right image texture
- **Metallic** at 0
- Transparency properly supported if needed, with **material settings > blend mode > alpha clip**
	- In the shading workspace, link the image alpha to the material alpha slot.

![Blender material alpha blend mode settings](mod_tut_advanced_screenshot5.jpg)  
*Set blend mode to alpha clip to simulate FFXI's alpha blending.*
{.annotation}

![Blender node editor for connecting alpha nodes](mod_tut_advanced_screenshot6.jpg)  
*Connect the alpha nodes*
{.annotation}

Your transparency should now work:  
![Working transparency in Blender](mod_tut_advanced_mithra4.png)

Now you're free to hide the bones. :D

One last step: switch the render pass to diffuse colour in order to not let the viewport light influence your perception of the texture.

![Setting render pass to diffuse light](mod_tut_advanced_screenshot7.jpg)

Time for the most fun part: texturing with Blender! :D

## Texture Painting
Things up to now were quite technical, but we're about to use Blender's greatest strength and weakness: its existence as a tool that tries to do literally everything.

Let's get started!

### 1. 3D Viewport & UV Editor Texturing
I recommend switching to the UV editing workspace for this, as it makes reloading textures easier.

Your bread and butter will be the texture painting mode, which is accessed with **ctrl+tab > top right**.
This allows you to draw directly on the model itself, and on the texture in the UV editor.

![Blender UV editing workspace and texture painting mode](mod_tut_advanced_screenshot8.jpg "Blender UV Editing workspace")

One of the upsides to painting directly in the viewport is avoiding UV distortion (but not always because UVs can just be bad). 
Notice in the UV editor of the screenshot above, the drawn heart is slightly distorted near the face marking.  
Therefore, I recommend doing as much editing to your texture in the 3D viewport painter first before taking it to your 2D painter.

Unfortunately as Blender does not natively have a layer system for texture painting mode, things like face tattoos which are best done on a separate layer should be painted as guides and traced over instead.

We also have another option for indirectly painting on the 3D model: **Texture projection**.  
It's called **Quick Edit** in Blender, and you'll see it in the screenshot above on the right side near the bottom. It allows you to send the current view to your 2D painter and do paintovers. However, it has a few downsides.

To get the least amount of distortion possible, it's best to use an orthographic view, which has problems of its own.   
Bad or very efficient UVs (eg. stacked, mirrored – very common in games) can also result in unpredictable projections.

![Blender quick edit sent to Krita](mod_tut_advanced_screenshot9.jpg)  
*Example of projection painting (quick edit) in Krita*
{.annotation}

![Unpredictable projection painting in Blender: UV editing workspace](mod_tut_advanced_screenshot10.jpg)  
![Result of projection painting/quick edit](mod_tut_advanced_screenshot11.jpg)  
*An example of unpredicatable projection, influenced by the orthographic view and mirrored UVs. Take note of the projection resolution as well - it's quite low but could be improved by zooming in a bit more in the viewport before sending it to the painting program.*
{.annotation}

Texture projection is therefore best used on simpler, more geometric objects like static props that have decent orthographic views.

Once you're done editing your texture in the 3D viewport,
go to the **UV editor > image > save a copy**  
You can then take this to your 2D painter :)

If you hit **alt+R** in the 2D view or '**reload textures**' (textools) now, you'll restore the original texture.

Before moving onto the next part, make sure you have your upscaled image in the base colour slot as this will make the next few steps easier.  
To more efficiently 2D texture, export the UV layout of the model.
- Enter edit mode
- Enable sync at the top left
- Select all UV shells in the UV editor (**A**)
- Go to **UV > export UV layout**, and save it as a separate file

If you had a vanilla texture in your base colour slot, you'll need to adjust the output size of the UV layout image during export.

The UV layout will tell you exactly where you need to paint and where you don’t. More on this in the next section :D

### 2. 2D Painting and Preview Workflow
![My typical setup in Krita for texture editing](mod_tut_advanced_screenshot12.jpg)  
*This is my typical setup* :)  
{.annotation}

*Alas, the trick is revealed... I already did this texture before LOL* 
{.smalltext}

I manually scale the original ripped texture first to serve as reference when needed, and then import the upscaled texture and UV layout.  
I usually import the Blender-edited texture in as a file layer too :D this way I don't need to constantly reimport the texture.  
Having the UV layout there saves a lot of time as it shows exactly what areas of the texture can be left unpainted. 

> ***Tip***
>
> It's good practice to extend your texture a few pixels beyond the UV shell border. This gives you very clean and professional texture padding :D the rest can be filled with a darker flat colour.

While this part is all up to you and your mad painting skills, you will still need to preview the texture in Blender. You could use AltanaView, but Blender is much faster since you don’t need to keep reinjecting with TexHammer.

To demonstrate sending the texture to Blender, I'll change her hair and eye colour.

![example texture](mod_tut_advanced_screenshot13.jpg "Example")

Save your texture as a separate .tga or .png, and then use it as the material base colour in Blender:

![Importing the example texture into Blender](mod_tut_advanced_screenshot14.jpg)

In a perfect world, you'd be done with your texture in five minutes and only need to therefore load your texture the first time.  
However, changes are inevitable. You could save your modified image and then reload the file by going through the material base colour slot again, but hitting **alt+R** to reload one texture in the UV editor, or **Reload textures** (TexTools) to reload all textures in the scene is so much faster.

![Blender texture reload options](mod_tut_advanced_screenshot15.jpg)

If you truly want to be one of the artists of all time though, you could restart Blender every single time you wanted to reload a texture... LOL

#### One more small thing!  
Remember the bones we hid? They can actually be very useful for texturing, as they let you get into places that would be very obscure otherwise!  
Character mouths are one of those places - sounds incredibly weird, but not even the vanilla textures do them right!  
To control them, enter pose mode and rotate them.

![Hume male 4A horrendous vanilla mouth texture](mod_tut_advanced_screenshot16.jpg "Hume male 4A")
![Elvaan male 5A horrendous vanilla mouth texture](mod_tut_advanced_screenshot17.jpg "Elvaan male 5A")

# ***DISGUSTING!!***
{.center}
Ahem... those seams are really something else! :'D
{.center}

---
## That concludes this tutorial series! :partying_face:
If you have any questions about any of the stages, or need more in-depth explanations, feel free to contact me and I'll answer to the best of my ability.

Please remember to save your work frequently! Saving incrementally (something Blender unfortunately lacks) is even better :)  
3D programs enjoy spontaneously combusting pretty frequently, so good saving practices are highly recommended (lol).

Now go make your crispy, epic textures! :D

When you're done with your texture, reinject it as per the [easy workflow](/website/posts/dat_modding_tutorial_basic/#1-injecting-with-texhammer). You won't need AltanaView this time, because Blender does it better :)

# *Happy modding!* :wave:

---

## Troubleshooting
**I want to keep bones visible in Blender, but the current shape they have is kinda distracting since they're so chunky. Is it possible to change this?**

Yes, it's possible.  
Select your skeleton/armature (can 3D programs just use the same terminology please?!?!) > choose the data tab (red running man) > viewport display > octahedral to something like stick.
{.answer}

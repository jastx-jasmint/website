+++
title = "[FFXI] Basic DAT Modding Tutorial"
description = "DAT mod tutorial: basic"
date = 2023-09-23T22:25:10+10:00
draft = false
tags = ["Final Fantasy XI", "Final Fantasy 11", "Modding", "Final Fantasy", "Art", "Tutorials"]
toc = true
+++

This tutorial series will go over my easy DAT modding workflow for ripping, modding, previewing and injecting textures into Final Fantasy XI.

For this tutorial, I'll use Kam'lanaut to demonstrate:  
{{< eagerimg image="mod_tut_easy_kamlanaut1.png" alt="Kam'lanaut vanilla vs. upscaled" >}}

n.b: This workflow has been tested on Windows and Linux, but not MacOS.

{.smalltext}

Also, you can click on the images to view their full size versions. If you right-click and open the image in a new tab, you'll end up looking at the resized. .webp images.  
Try it out with Kam'lanaut's picture above! :point_up:  
Pretty cool, huh? ;D

With that said, let's begin! :tea:

# 1. Ripping Textures
## Prerequisites
- A final Fantasy XI installation
- [AltanaView](https://github.com/CAProjects/AltanaViewer)
- [TexHammer](https://korithsmods.blogspot.com/p/tex-hammer.html)
- [chaiNNer (optional)](https://github.com/chaiNNer-org/chaiNNer)
- Image editing software of your choice (optional)
- Photoshop DDS plugin from [Amelila's Mod Creators Pack](https://goo.gl/kV52t6) ([ mirror](https://mega.nz/folder/kV4TyJSC#UVPaDduovK_34DhwTUwNPA))  
 or [GIMP DDS plugin](https://code.google.com/archive/p/gimp-dds/downloads) (this tutorial will use GIMP)
- Time and patience :)

### 1. AltanaView
![AltanaView UI overview](mod_tut_easy_screenshot1.jpg)

> ***For the Linux Users***
>
> Make sure to run Wine with WineD3D (not DXVK!), otherwise AltanaView will not work!
> If you're having any strange issues with mouse input lag, try a different version of Wine (eg. Lutris-GE).


**Area 1**  
Choose the asset you wish to modify.
You can also change the background colour and floor type here.

**Area 2**  
The information bar, where the crucial info lives.
When you select your asset, notice that the bottom left points to a directory. This is where you'll find the DAT file you need to rip from.

![AltanaView information bar](mod_tut_easy_screenshot2.jpg)

### 2. TexHammer
![TexHammer UI overview](mod_tut_easy_screenshot3.jpg)  
	1. Navigate to and load the DAT file previously shown by AltanaView.
	2. Choose the texture(s) you would like to modify.
	3. Hit export. This will then pop up:

![TexHammer alpha option](mod_tut_easy_screenshot4.jpg)  
Either option is okay, but in come cases, the clamped alpha may work better. See what works in your case.
Export to your desired location.

The following section is optional but should still be given consideration if you only wish to recolour your texture. This is because the beauty of upscaling doesn't stop at higher resolution textures for minimal effort - it can also be used to remove compression artefacts without changing the dimensions of the image. :)


# 2. Upscaling
## Upscaling with chaiNNer

![chaiNNer image processing GUI](https://github.com/chaiNNer-org/chaiNNer/raw/main/docs/assets/banner.png)

This tutorial will go over AI upscaling textures one-by-one, which will help you nail the basics of the tool :)

You will also need some upscaling models to work with, which you can get from the [openmodelDB](https://openmodeldb.info/) and the [Upscale Wiki](https://upscale.wiki/w/index.php?title=Model_Database&oldid=1571). The chaiNNer README page contains some links as well.

**Recommended models**:  
- [BC1 Smooth 2](https://openmodeldb.info/models/1x-BC1-smooth2): Removes DXT compression from DDS images, which is the format used by FFXI
- [ESRGAN 4x](https://openmodeldb.info/models/4x-ESRGAN): Fits most things. Good to use as a baseline
- [HDCube](https://openmodeldb.info/models/4x-HDCube3): Great for details, fur and hair, but can change colours due to the heavy stylisation
- [Trixie 4x](https://openmodeldb.info/models/4x-Trixie): Good for armour and some details
- [SkyrimTexV2_Fabric](https://openmodeldb.info/models/4x-SkyrimTex-v2-Fabric): Really good for details and clothing. It's more on the realistic side, which matches well with FFXI. Check out the other Skyrim models too :)

### UI Overview

![chaiNNer UI overview](mod_tut_easy_screenshot5.jpg)

Before we start, please download the dependencies using the download button at the top right. If you don't, you won't have the nodes available to replicate my setup.

**Left pane**  
Here is where you can drag out nodes into the middle workspace. Your bread and butter will be:
- Load image
- Upscale image (PyTorch)
- Text append (for file names)
- Save image

**Top bar**  
The controls, gauges and settings/dependencies bar.
Here you can run, stop and pause upscales. It's better to learn the hotkeys though :D

**Menu bar**  
The basics can be found here, like saving your chaiNNer template file. There's a lot of hints here, and helpful links in the help section.

It might look a bit complex and like spaghetti now, but chaiNNer is very straightforward once you figure it out. :D

### Node Setup  
![chaiNNer single iteration node setup](mod_tut_easy_screenshot6.jpg)

I think the image is rather self-explanatory, but to explain the logic:
- Load your image and upscale model
	- For this first one, use a 1x decompressing model sucha s the BC1 one mentioned previously in this tutorial. This ensures that the rest of the process works with a clean image

- Upscale the image, connecting the image and model to this node
- Load more models.
	- This time, you'll want to use a model that is focused on bigger resolutions such as ESRGAN 4x.
	
- Upscale the image again, connecting the output from the previous upscale to the image slot, and the new model to the model slot
- View image nodes: these give you a quick preview of the upscaled image. 
	- Not entirely necessary but they look nice :D

- Set up your file naming convention. This is done with the text append node, which takes the name of the original texture file, and appends the 4x model name. 

- Connect the output text to the image name slot in the 'save image' node.

The subdirectory path is optional but I like to keep my upscales separate from the original textures.

***With this, you should have some nicely upscaled textures!***

![Kam'lanaut clothing upscaled texture](mod_tut_easy_kamlanaut2.png "4x texture upscale")

Now you're all set for the fun part: **editing**!

I don't need to walk you through this one :D 
But since we're on the topic of AI upscales, consider utilising multiple models and mixing the outputs in your program of choice to get interesting results. You can do this with transparency masks.  
This is especially useful for NPCs who wear cloth, leather and/or metallic things, like Kam'lanaut.
Qultada is a good example too :)

Next we'll learn how to preview your modified textures with TexHammer and AltanaViewer.

## Texture Padding and File Conversion
Now that you have your upgraded texture, we need to convert it
to a format usable by the game.  
FFXI utilises .DDS files for textures, which is a lossy format.
However, DDS is not commonly supported nowadays, which is where the DDS plugin comes in.

This tutorial will use the [GIMP DDS plugin](https://code.google.com/archive/p/gimp-dds/downloads) and will go over the required settings. (These settings are applicable for PS users)

...But first, let me cover an important concept in texturing:

### 1. Texture Padding
This isn't about sponges, but about the colour of transparent pixels.

> "Why does this matter? Can transparent pixels even have colour?"

In the case of textures with transparency, yes!

Here's an example of a texture with white texture padding vs. one with proper texture padding:

![Kam'lanaut bad texture padding example](mod_tut_easy_kamlanaut4.png) 
*Bad example*
{.annotation}

![Kam'lanaut good texture padding example](mod_tut_easy_kamlanaut5.png)  
***Good example***  
*There's no need to make the padding area this detailed - most of the time, a flat, darker colour from the texture will work just fine.*
{.annotation}

Most painting programs will automatically save the padding colour as either black or white.

You should hope and pray that it's black, but most of the time, it gets saved as white, which results in weird frosty bits as in the bad example. Q_Q

> ***Fun fact*** 
>
> Early in development, the texture artists at Square Enix used pure black for texture padding, and only later started to use other colours. Only using black leads to lower quality textures, as it still shows (though not as obviously as white of course).

No matter how clean you make your alphas, white padding always finds ways to show, *especially with mipmapping enabled.*

> ***"What exactly is mipmapping, and why should I care?"***
>
> Mipmapping is usually a big performance and aesthetic saver in games, as it makes the game use lower resolution textures for objects further away from the player. This simulates the effect of objects further away looking less detailed. Without mipmapping, textures far away remain at their full level of detail, resulting in a shimmering effect as you play the game.
>
> For the texture artist, the downside to this resolution lowering (especially in textures with transparency/alphas) is that sometimes the transparency will bleed into areas it shouldn't. This is why texture padding matters: if the transparency bleeds where it shouldn't and leads to awful edges, there's a backup colour to use. Basically, if you don't do texture padding properly, at best people will realise something is a bit off. At worst, people will know you're a noob!

This is mainly a problem if you edited your textures - if you only care about upscaling, the upscaler also scales the padding area for you, so you can simply export and be done with it.

This is the only part of editing that I will teach, because a lot of software (painting ones especially) don't support the preservation of texture padding colour.  
GIMP and PS do, so I'll show you how to do it in GIMP.
(PS does it in a much more intuitive manner, by the way - refer to [Amelila's guide](https://youtu.be/EpA_RzFulAs?si=U4Mn5hh7swAkopCk&t=562)). Amelila also talks about texture padding at the timestamp, but doesn't call it that. :wink:

### 2. Texture Padding in GIMP
With your texture in GIMP, **alt-click** the layer thumbnail and invert the selection with **ctrl+i**.  
![GIMP selection inversion](mod_tut_easy_screenshot7.jpg)

**Fill** with your desired colour (ideally a dark colour).  
![GIMP texture padding fill](mod_tut_easy_screenshot8.jpg)

**Shift-click** the layer thumbnail, and **fill** the selected area with solid black (this is a quick mask).  
![GIMP quick mask](mod_tut_easy_screenshot9.jpg)

Now you're ready to export to .DDS!


### 3. DDS Export Settings
Hit **ctrl+E** (or file > export) and change the file extension to .dds .

You will then be presented with a pop-up:  
![GIMP DDS export settings for FFXI](mod_tut_easy_screenshot10.jpg)

**The important settings are:**  
1. Compression (set to **DXT3**)
2. Mipmaps (**generate mipmaps**)
{.indent}

Then hit export. :)

Congratulations! Now you're ready to inject your texture into the .DAT file it came from.

## Injection and Previewing with AltanaView
We're almost done! Kudos to you for making it this far :D

### 1. Injecting with TexHammer
Yes, we're reuniting with our friend the hammer again!

1. Import your .DAT file that you initially exported the textures from. 
2. You can then drag your .DDS file on the thumbnail/tiles where the original textures in the .DAT file are, and it will automatically update the resolution for you.
3. Save your .DAT file.
{.indent}

![TexHammer DAT file injection with wine explorer](mod_tut_easy_screenshot11.jpg)

> ***For the Linux Users*** 
>
> Use Wine Explorer to drag textures into TexHammer. You can easily access this in Lutris.
>
> `WINEPREFIX='...' wine explorer`
>
>I also recommend creating symlinks of your folders to `./drive_c/users/you/Favourites`.
>
> `You in ./Favourites: ln -s /texture/file/path`

In order to preview your texture in AltanaView however, you need to give your file the original name.  
What you should do instead is change the filename of the original .DAT file, and then give your modded DAT the original name.

### 2. Previewing with AltanaView
Once you get back in and find the asset you originally chose, it should have the new textures.

![AltanaView preview of new textures](mod_tut_easy_screenshot12.jpg)

You can also reinject and save in real-time, and AltanaView should update the textures for you. If it doesn't happen instantly, either change tabs in the viewer or restart the application.

That covers everything involved with the AltanaView texture workflow for FFXI DAT modding.

---

At this point, you can just leave your modded DAT in the original folder and play the game with your nice mod (unless you have an XIpivot overlay that "overwrites" that texture).  
Speaking of XIpivot, go create your own overlay folder and put the modded DAT in there instead! 
This way, you won't need to fear any updates potentially overwriting your DAT file with the vanilla versions. :)

If you have any questions feel free to shoot them over and I'll do my best to answer them. You'll have the best chance of getting a prompt response via email and Discord (if you have it).

# Happy modding! :D
Once you're ready for another two 3D programs (one of them is quite easy to use, the other is a blender), maybe try out the [advanced series](/website/posts/dat_modding_tutorial_advanced)... ;)

---

## Troubleshooting
**I have FFXI installed but AltanaView says something about a list not being found. What do I do?**  

On **Windows**, this is sometimes fixed by creating a shortcut to AltanaView and running the shortcut. This can be via the taskbar or a shortcut file.  
If that doesn't work, try creating a shortcut to the list folder and putting it wherever AltanaView is searching.  
...  
On **Linux**, you will need to create a symlink to the list folder wherever AltanaView is trying to look (most likely your home directory).
This can also be caused by not having AltanaView in your Wine prefix. Sticking it in the C:\ drive should eliminate this problem.  
If you're still having trouble, feel free to reach out and I'll see what's up.
{.answer}

**In chaiNNer, I don't have the nodes that you have. What do?**

Make sure to download the required dependencies using the download button at the top right corner. The one shown in this tutorial is from PyTorch.
{.answer}

**I've saved my file as a DDS but TexHammer won't accept it. What's wrong?**  

Check that you exported it as a BC2/DXT3 file. TexHammer won't accept it otherwise.
{.answer}

# Website code of https://jastx-jasmint.gitlab.io/website/


This website was built using Hugo & the m10c theme.

If you have any improvements or suggestions, put in a merge request and I'll have a look :) this can include small things like typos, all the way down to big, shiny new features.

I'm not a web developer or even a programmer (in fact I'm super new to actually making a website lol), so any help will be much appreciated!
